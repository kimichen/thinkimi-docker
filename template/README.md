# Template

  This is the template Dockerfile for gradle build java project.
  
# Configuration

  * Copy the `Dockerfile` to the root of gradle build java project.
  * Run `./gradlew clean bootDistTar` to build the tar file of the project.
  * Change the arguments defined in the `Dockerfile`, include `maintainer`, `version`, `FILE_NAME`.
  * Change the expose port(defined in `application.yml`) and entrypoint(usually the pattern is `bin/${rootProject.name}`).
  
# Build image and tags.

  * Use the docker command to build and tag.
  ```shell
  # if skip the version, it will be tagged to "lastest"
  docker build -t thinkimi/${repository.name}:${version} .
  ```
  * Login to the private registry.
  * Push the docker image to the private registry.
  ```shell
  docker push ${registry.url}/thinkimi/${repository.name}:${version}
  ```
  
# Pull the image on the server and run it.

  Use the project related `docker-compose.yml` file defined in [thinkimi-docker](https://gitlab.com/kimichen/thinkimi-docker "thinkimi-docker") project to run on the server.
