# Thinkimi Docker Registry

The project for docker register. Included https and basic auth support.

## Getting Started

These instruction will show how to configure a private docker register with https and basic auth support.

### Configuration

After clone [this repository](https://gitlab.com/kimichen/thinkimi-docker-registry), you show `mkdir data` under the main directory, which will be mounted to docker.

#### Https Support

You can get all the instructions for many kinds of environment from [Certbot Official Website](https://certbot.eff.org/), here I use [certbot-auto](https://certbot.eff.org/docs/install.html#certbot-auto) command.

```shell
certbot-auto --nginx -d registry.example.com
```

You should replace `registry.example.com` with your really domain  which already points to your system ip.

#### Basic Auth Support

1. Generate the password file.

    ```shell
    sudo sh -c "echo -n 'thinkimi:' > nginx.htpasswd"
    sudo sh -c "openssl passwd -apr1 >> /etc/nginx/conf/nginx.htpasswd"
    ```
    You can replace `thinkimi` with your own username. 

1. Add the follow two line to the nginx config file under `server` > `location` section.

    ```
    auth_basic "Registry realm";
    auth_basic_user_file /etc/nginx/conf/nginx.htpasswd;
    ```

### Built With

* [Docker](https://docs.docker.com/) - The runtime environment
* [Nginx](https://nginx.org/en/docs/) - The proxy server
* [Certbot](https://certbot.eff.org/) - The automatical util to use [Let’s Encrypt](https://letsencrypt.org/) certificates on manually-administrated websites to enable HTTPS.

## Authors

* **Kimi Chen** - *All the works* - [KimiChen](https://gitlab.com/kimichen)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
