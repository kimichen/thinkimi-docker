# Thinkimi Alitas DB

## Configuration

  * Create the password file named `postgres_password.txt` match the `docker-compose.yml`.
  * Make backup directory, and configure in the `backup/pg_backup.config`.
  
## Backup

  Use `crontab` to backup just add the following line.
  ```
  0 1 * * * {backup/pg_backup_rotated.sh} -c ${backup/pg_backup.config} > /dev/null
  ```
