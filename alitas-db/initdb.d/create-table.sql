-- Split schema for tables.

CREATE USER sec WITH PASSWORD 's9e1c0913';
CREATE USER customer WITH PASSWORD 'c9u1s0t9o1m3er';
CREATE USER product WITH PASSWORD 'p9r1o0d9u1c3t';
CREATE USER ord WITH PASSWORD 'o9r1d0e9r13';

CREATE SCHEMA AUTHORIZATION sec;
CREATE SCHEMA AUTHORIZATION customer;
CREATE SCHEMA AUTHORIZATION product;
CREATE SCHEMA AUTHORIZATION ord;

ALTER TABLE users SET SCHEMA sec;
ALTER TABLE authorities SET SCHEMA sec;
ALTER TABLE oauth_client_details SET SCHEMA sec;
ALTER TABLE persistent_logins SET SCHEMA sec;

GRANT ALL ON ALL TABLES IN SCHEMA sec TO sec;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA sec TO sec;

ALTER TABLE tk_customer SET SCHEMA customer;
ALTER TABLE tk_document SET SCHEMA customer;
ALTER TABLE tk_folder SET SCHEMA customer;
ALTER TABLE tk_country SET SCHEMA customer;
ALTER TABLE tk_drive_data SET SCHEMA customer;

GRANT ALL ON ALL TABLES IN SCHEMA customer to customer;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA customer TO customer;

ALTER TABLE tk_product SET SCHEMA product;
ALTER TABLE tk_product_slug SET SCHEMA product;
ALTER TABLE tk_slug SET SCHEMA product;
ALTER TABLE tk_sub_product SET SCHEMA product;
ALTER TABLE tk_tax SET SCHEMA product;
ALTER TABLE tk_languages SET SCHEMA product;
ALTER TYPE t_slug SET SCHEMA product;

GRANT ALL ON ALL TABLES IN SCHEMA product to product;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA product TO product;


-- Postgresql tips.

SELECT
  type.typname AS name,
  string_agg(enum.enumlabel, '|') AS value
FROM pg_enum AS enum
JOIN pg_type AS type
  ON (type.oid = enum.enumtypid)
GROUP BY type.typname;          -- Show all the enums.

-- Definition tables for sec.

CREATE TABLE users(
       username VARCHAR(256) NOT NULL PRIMARY KEY,
       password VARCHAR(256) NOT NULL,
       enabled BOOLEAN NOT NULL
);

CREATE TABLE authorities (
       username VARCHAR(256) NOT NULL,
       authority VARCHAR(256) NOT NULL,
       FOREIGN KEY (username) REFERENCES users(username)
);
CREATE UNIQUE index ix_auth_username ON authorities (username, authority);


INSERT INTO users(username, password, enabled)
VALUES ('kimi@thinkimi.com', '$2a$10$mta.btAlC5V0alH63j2LM.fBg7qjWGV7t.gmptv7HvDiM4ecyFplm', TRUE),
('think@thinkimi.com', '$2a$10$QzAhq9WTBOUGLTU1ahqGx.KqId.KIidJ0WerUJdYnbqj/7ua/kdvC', TRUE);

INSERT INTO authorities(username, authority)
VALUES ('kimi@thinkimi.com', 'ADMIN'), ('think@thinkimi.com', 'USER');


CREATE TABLE persistent_logins (
    username VARCHAR(256) NOT NULL,
    series VARCHAR(256),
    token VARCHAR(256) NOT NULL,
    last_used TIMESTAMP NOT NULL,
    PRIMARY KEY (series)
);

CREATE TABLE oauth_client_details (
    client_id VARCHAR(256),
    resource_ids VARCHAR(256),
    client_secret VARCHAR(256),
    scope VARCHAR(256),
    authorized_grant_types VARCHAR(256),
    web_server_redirect_uri VARCHAR(256),
    authorities VARCHAR(256),
    access_token_validity INTEGER,
    refresh_token_validity INTEGER,
    additional_information VARCHAR(4096),
    autoapprove VARCHAR(256),
    PRIMARY KEY(client_id)
);


INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types, web_server_redirect_uri, autoapprove)
VALUES ('AdminSystem', '{bcrypt}$2a$10$a8FloHdOudo9DR7rKaIZYO10oX7pl13Bm1HCz7DWYzmSxwzF9dgZ.', 'user_info', 'client_credentials', 'http://localhost:9091/sso/hello', 'user_info');

-- Definition tables for customer.

CREATE TABLE tk_country(
       code             CHAR(2),
       desc_en          VARCHAR(50),
       desc_cn          VARCHAR(30),
       PRIMARY KEY (code)
);

CREATE TABLE tk_folder(
       id               SERIAL,
       account          VARCHAR(40),
       account_type     VARCHAR(40),
       surname          VARCHAR(40),
       name             VARCHAR(40),
       full_name        VARCHAR(40),
       phone            VARCHAR(20),
       email            VARCHAR(20),
       address          VARCHAR(100),
       create_time      TIMESTAMP DEFAULT current_timestamp,
       last_update_time TIMESTAMP DEFAULT current_timestamp,
       PRIMARY KEY (id)
);

CREATE TABLE tk_customer(
       id               SERIAL,
       folder_id        INTEGER,
       surname          VARCHAR(40),
       name             VARCHAR(40),
       full_name        VARCHAR(40),
       full_name_local  VARCHAR(40),
       full_name_ru     VARCHAR(40),
       birth            DATE,
       gender           VARCHAR(10),
       country          CHAR(2),
       create_time      TIMESTAMP DEFAULT current_timestamp,
       last_update_time TIMESTAMP DEFAULT current_timestamp,
       PRIMARY KEY (id),
       FOREIGN KEY (folder_id) REFERENCES tk_folder(id),
       FOREIGN KEY (country) REFERENCES tk_country(code)
);

CREATE TABLE tk_document(
       id               SERIAL,
       customer_id      INTEGER,
       doc_number       VARCHAR(20),
       doc_type         VARCHAR(20),
       expire_date      DATE,
       valid            BOOL,
       create_time      TIMESTAMP DEFAULT current_timestamp,
       last_update_time TIMESTAMP DEFAULT current_timestamp,
       PRIMARY KEY (id),
       FOREIGN KEY (customer_id) REFERENCES tk_customer(id)
);

CREATE TABLE tk_drive_data
(
    id               SERIAL,
    drive_id         VARCHAR(100),
    name             VARCHAR(40),
    parent           VARCHAR(100),
    folder_id        INTEGER,
    suggest_name     VARCHAR(40),
    checked          BOOL,
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id),
    FOREIGN KEY (folder_id) REFERENCES tk_folder (id)
);



-- Definition tables for product.


CREATE TABLE tk_languages
(
    id               SERIAL,
    -- locale           VARCHAR(10),
    message_key      TEXT UNIQUE,
    message_content  TEXT[],
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id)
);

CREATE TABLE tk_support_lang
(
    id               SERIAL,
    locale           TEXT,
    index            INTEGER,
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id)
);

INSERT INTO tk_support_lang (locale, index) values
('en', 1), ('zh', 2);


CREATE TABLE tk_product
(
    id               SERIAL,
    name             TEXT,
    description      TEXT,
    slugs            INTEGER[],
    image            TEXT,
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id)
);

INSERT INTO tk_product (name, description, slugs, image) VALUES
('product.lighthouse.name', 'product.lighthouse.desc', ARRAY[1, 3], 'lighthouse.jpg'),
('product.petiterocket.name', 'product.petiterocket.desc', ARRAY[1, 3], 'petiterocket.jpg');

INSERT INTO tk_languages (message_key, message_content) VALUES
('product.lighthouse.name', ARRAY['Lighthouse', '灯塔']),
('product.lighthouse.desc', ARRAY['Use lighthouse to see all of the world.', '通过灯塔才能看见整个世界。']),
('product.petiterocket.name', ARRAY['Petite Rocket', '小火箭']),
('product.petiterocket.desc', ARRAY['Util to use lighthouse .', '使用灯塔的工具。']);


CREATE TABLE tk_sub_product
(
    id               SERIAL,
    product_id       INTEGER,
    name             TEXT,
    image            TEXT,
    retail_price     NUMERIC(10, 2),
    taxes            INTEGER[],
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id),
    FOREIGN KEY (product_id) REFERENCES tk_product(id)
);

INSERT INTO tk_sub_product (product_id, name, image, retail_price) VALUES
(1, 'lh.subproduct.month', 'lighthouse.jpg', 2.99),
(1, 'lh.subproduct.quarter', 'lighthouse.jpg', 8.99),
(1, 'lh.subproduct.year', 'lighthouse.jpg', 29.99),
(2, 'pr.subproduct', 'petiterocket.jpg', 3.49);

INSERT INTO tk_languages (message_key, message_content) VALUES
('lh.subproduct.month', ARRAY['Monthly', '每月付']),
('lh.subproduct.quarter', ARRAY['Quarterly', '每季度付']),
('lh.subproduct.year', ARRAY['Yearly', '每年付']),
('pr.subproduct', ARRAY['Petite Rocket', '小火箭']);

CREATE TABLE tk_tax
(
    id               SERIAL,
    name             TEXT,
    rating           INTEGER,
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id)
);

CREATE TYPE t_slug AS ENUM ('COLLECTION', 'CATEGORY');

CREATE TABLE tk_slug
(
    id               SERIAL,
    type             t_slug,
    name             TEXT,
    slug             TEXT,
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id),
    FOREIGN KEY (name) REFERENCES tk_languages(message_key)
);

INSERT INTO tk_languages (message_key, message_content) VALUES
('slug.util', ARRAY['Util', '工具']),
('slug.consult', ARRAY['Consult', '咨询']),
('slug.new_in', ARRAY['New In', '最新']);

INSERT INTO tk_slug(type, name, slug) VALUES 
('CATEGORY', 'slug.util', 'util'),
('CATEGORY', 'slug.consult', 'consult'),
('COLLECTION', 'slug.new_in', 'new_in');


-- Definition tables for order.

CREATE TABLE tk_order_request
(
    id               SERIAL,
    order_id         TEXT,
    first_name       TEXT,
    last_name        TEXT,
    email            TEXT,
    phone            TEXT,
    order_total      NUMERIC(10, 2),
    shipping_address JSONB,
    billing_address  JSONB,
    items            INTEGER[],
    create_time      TIMESTAMP DEFAULT current_timestamp,
    last_update_time TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (id)
);
